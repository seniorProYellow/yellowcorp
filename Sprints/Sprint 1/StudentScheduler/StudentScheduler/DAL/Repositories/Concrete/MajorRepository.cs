﻿using System.Collections.Generic;
using System.Linq;
using StudentScheduler.DAL.Repositories.Concrete.Patterns;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.Models;

namespace StudentScheduler.DAL.Repositories.Concrete
{
    public class MajorRepository : Repository<Major>, IMajorRepository
    {
        public MajorRepository(SchedulerContext context)
            : base(context)
        {
        }

        private SchedulerContext SchedulerContext
        {
            get { return Context as SchedulerContext; }
        }

        // Specialized Methods Below:

        public IEnumerable<Major> GetMajorsForSchool(int schoolId)
        {
            return SchedulerContext.Majors.Where(m => m.SchoolID == schoolId).ToList();            
        }

    }
}