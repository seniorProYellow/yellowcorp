﻿using StudentScheduler.DAL.Repositories.Concrete.Patterns;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.Models;

namespace StudentScheduler.DAL.Repositories.Concrete
{
    public class SchoolRepository : Repository<School>, ISchoolRepository
    {
        public SchoolRepository(SchedulerContext context)
            : base(context)
        {
        }

        private SchedulerContext SchedulerContext
        {
            get { return Context as SchedulerContext; }
        }

        // Specialized Methods Below:

    }
}