﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace StudentScheduler.DAL
{   
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        private SchedulerContext db = new SchedulerContext();

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("UserFullName", UserFullName?.ToString() ?? UserName.ToString()));
            if (SchoolId != null)
            {
                userIdentity.AddClaim(new Claim("SchoolId", SchoolId?.ToString() ?? "")); 
                userIdentity.AddClaim(new Claim("SchoolName", db.Schools.Find(SchoolId)?.Title.ToString() ?? ""));
            }
            if (MajorId != null) { userIdentity.AddClaim(new Claim("MajorId", MajorId?.ToString() ?? "")); }
            if (GoogleCalendarId != null) { userIdentity.AddClaim(new Claim("GoogleCalendarId", GoogleCalendarId?.ToString() ?? "")); }
            if (UserLogo != null) { userIdentity.AddClaim(new Claim("UserLogo", UserLogo?.ToString() ?? "")); }
            return userIdentity;
        }
        public string UserFullName { get; set; }
        public int? SchoolId { get; set; }
        public int? MajorId { get; set; }
        public string GoogleCalendarId { get; set; }
        public string UserLogo { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            // Disable code-first migrations
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}