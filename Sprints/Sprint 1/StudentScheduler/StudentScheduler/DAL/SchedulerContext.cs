namespace StudentScheduler.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using StudentScheduler.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public partial class SchedulerContext : DbContext
    {

        public SchedulerContext()
            : base("name=SchedulerContext")
        {
            // Disable code-first migrations
            Database.SetInitializer<SchedulerContext>(null);
        }

        public virtual DbSet<Completion> Completions { get; set; }
		public virtual DbSet<Cours> Courses { get; set; }
        public virtual DbSet<Major> Majors { get; set; }
		public virtual DbSet<MajorRequirement> MajorRequirements { get; set; }
        //public virtual DbSet<RequirementRule> RequirementRules { get; set; }
        public virtual DbSet<Requirement> Requirements { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<sysdiagram> Sysdiagrams { get; set; }
        public virtual DbSet<UniversityEvent> UniversityEvents { get; set; }
        //public virtual DbSet<UserAccount> UserAccounts { get; set; }
		public virtual DbSet<UserMajorRequirement> UserMajorRequirements { get; set; }
        public virtual DbSet<UserSchedule> UserSchedules { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cours>()
                .Property(e => e.CourseTitle)
                .IsUnicode(false);

            modelBuilder.Entity<Cours>()
                .Property(e => e.CourseSubtext)
                .IsUnicode(false);

            modelBuilder.Entity<Cours>()
                .Property(e => e.CourseNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Cours>()
                .Property(e => e.CourseDays)
                .IsUnicode(false);

            modelBuilder.Entity<Major>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Major>()
                .HasMany(e => e.Courses)
                .WithRequired(e => e.Major)
                .WillCascadeOnDelete(false);
				
			modelBuilder.Entity<Major>()
                .HasMany(e => e.MajorRequirements)
                .WithRequired(e => e.Major)
                .WillCascadeOnDelete(false);	
/*
            modelBuilder.Entity<Major>()
                .HasMany(e => e.Requirements)
                .WithRequired(e => e.Major)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Major>()
                .HasMany(e => e.RequirementRules)
                .WithRequired(e => e.Major)
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<RequirementRule>()
                .Property(e => e.RuleDescription)
                .IsUnicode(false);

            modelBuilder.Entity<RequirementRule>()
                .Property(e => e.RuleString)
                .IsUnicode(false);

            modelBuilder.Entity<Requirement>()
                .Property(e => e.RequirementTitle)
                .IsUnicode(false);

            modelBuilder.Entity<Requirement>()
                .Property(e => e.RequirementDescription)
                .IsUnicode(false);
*/
			modelBuilder.Entity<Requirement>()
                .Property(e => e.RequirementTitle)
                .IsUnicode(false);
				
			modelBuilder.Entity<Requirement>()
                .Property(e => e.RequirementDescription)
                .IsUnicode(false);	

			modelBuilder.Entity<Requirement>()
                .HasMany(e => e.MajorRequirements)
                .WithRequired(e => e.Requirement)
                .HasForeignKey(e => e.ReqID)
                .WillCascadeOnDelete(false);

			modelBuilder.Entity<Requirement>()
                .HasMany(e => e.UserMajorRequirements)
                .WithRequired(e => e.Requirement)
                .HasForeignKey(e => e.ReqID);

            modelBuilder.Entity<School>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<School>()
                .HasMany(e => e.Majors)
                .WithRequired(e => e.School)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<School>()
                .HasMany(e => e.UniversityEvents)
                .WithRequired(e => e.School)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UniversityEvent>()
                .Property(e => e.EventTitle)
                .IsUnicode(false);

            modelBuilder.Entity<UniversityEvent>()
                .Property(e => e.EventDescription)
                .IsUnicode(false);

            modelBuilder.Entity<UniversityEvent>()
                .Property(e => e.EventDays)
                .IsUnicode(false);
/*
            modelBuilder.Entity<UserAccount>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<UserAccount>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<UserAccount>()
                .Property(e => e.Identification)
                .IsUnicode(false);

            modelBuilder.Entity<UserAccount>()
                .HasMany(e => e.UserSchedules)
                .WithRequired(e => e.UserAccount)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete(false);
*/
            modelBuilder.Entity<UserSchedule>()
                .Property(e => e.ActivityTitle)
                .IsUnicode(false);

            modelBuilder.Entity<UserSchedule>()
                .Property(e => e.ActivityDescription)
                .IsUnicode(false);

            modelBuilder.Entity<UserSchedule>()
                .Property(e => e.ActivityDays)
                .IsUnicode(false);
        }

        public System.Data.Entity.DbSet<StudentScheduler.Models.AspNetRole> AspNetRoles { get; set; }

        public System.Data.Entity.DbSet<StudentScheduler.Models.AspNetUser> AspNetUsers { get; set; }

        public System.Data.Entity.DbSet<StudentScheduler.Models.Comment> Comments { get; set; }

        public System.Data.Entity.DbSet<StudentScheduler.Models.UserLogo> UserLogoes { get; set; }
    }
}
