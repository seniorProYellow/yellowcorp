﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Text;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using StudentScheduler.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System.Diagnostics;

namespace StudentScheduler.Controllers
{
    public class CoursController : Controller
    {
        private SchedulerContext db = new SchedulerContext();

        // GET: Cours
        public ActionResult Index()
        {
            var courses = db.Courses.Include(c => c.Major);
            var currentUserID = User.Identity.GetUserId();

            //Make a list of the current class IDs that the user is taking
            //(Then store it in the ViewBag)
            var userActivities = db.UserSchedules
                            .Where(u => u.UserID == currentUserID)
                            .Select(a => a.ActivityCourseID);

            if(userActivities.Count() > 0)
            {
                List<int?> templist = userActivities.ToList();
                ViewBag.UserActivities = templist;
            }
            
            

            return View(courses.ToList());
        }

        public int CheckIfContains(string RawDays, char Day)
        {
            if (RawDays.Contains(Day))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public string DaysToBits(string RawDays)
        {
            var builder = new StringBuilder();

            builder.Append(CheckIfContains(RawDays, 'U'));
            builder.Append(CheckIfContains(RawDays, 'M'));
            builder.Append(CheckIfContains(RawDays, 'T'));
            builder.Append(CheckIfContains(RawDays, 'W'));
            builder.Append(CheckIfContains(RawDays, 'R'));
            builder.Append(CheckIfContains(RawDays, 'F'));
            builder.Append(CheckIfContains(RawDays, 'S'));

            return( builder.ToString() );
        }

        public ActionResult GenerateSchedule()
        { 
            List<UserMajorRequirement> usermajorreqs = new List<UserMajorRequirement>();
            List<Cours> neededcourses = new List<Cours>();
            string userId = User.Identity.GetUserId();
            usermajorreqs = db.UserMajorRequirements.Where(m => m.UserID == userId && m.ReqMet == false).ToList();


            //Make a list of the current class IDs that the user is taking
            //(Then store it in the ViewBag)
            var userActivities = db.UserSchedules
                            .Where(u => u.UserID == userId)
                            .Select(a => a.ActivityCourseID);

            if (userActivities.Count() > 0)
            {
                List<int?> templist = userActivities.ToList();
                ViewBag.UserActivities = templist;
            }


            String stringy = "";
            String[] delimit = new string[3];
            String concat = "";


            //////////////////////////////////////
            /// GETS CLASSES FROM REQUIREMENTS //
            ////////////////////////////////////

            // Loops through the requirements table for the user and determines if
            // a given requirement is a class (as opposed to some other grad req)
            // If it is, it pulls the required class from the courses table and
            // adds it to a list.
            foreach (var entry in usermajorreqs)
            {
                stringy = entry.Requirement.RequirementTitle.ToString();
                
                //If the first two characters of the req are capital
                // (And not 'BS'), then it's probably a class
                if( (stringy[0] >= 'A' && stringy[0] <='Z' && 
                     stringy[1] >= 'A' && stringy[1] <= 'Z') &&
                     stringy.Substring(0,2) != "BS" )
                {
                    // Take the two parts of the class subtext (ie 'CS' and '160')
                    // and combine them ('CS160')
                    delimit = stringy.Split(' ');
                    concat = String.Concat(delimit[0], delimit[1]);

                    // Then add them to the users needed courses list.
                    if (db.Courses.Where(m => m.CourseSubtext.ToString() == concat).FirstOrDefault()!=null)
                    {
                        neededcourses.Add(db.Courses.Where(m => m.CourseSubtext.ToString() == concat).FirstOrDefault());
                    }
                }
            }

            ////////////////////////////////////////
            /// GET FIRST CLASS OF EACH SEQUENCE //
            ///////////////////////////////////////

            /// Since we don't have prerequisites tied to the classes, we need to
            /// improvise a little. Just assume that the lowest numbered class of
            /// each area are probably fine to take, I guess?? Like you need to take
            /// CS160 before 161... It doesn't account for things like prereqs from
            /// different areas, but without that being added to the database, there's
            /// not really a way to check for that...
             
            List<Cours> classesInMajor = new List<Cours>();
            List<Cours> generatedSchedule = new List<Cours>();

            foreach(var requiredclass in neededcourses)
            {
                Debug.WriteLine("");
                Debug.WriteLine("The subtext of " + requiredclass.CourseTitle + " is " + requiredclass.CourseSubtext);
                string major = "";
                int classnumber = 0;
                int lowestCourseNumber = 1000;

                
                //If the major digit is 2 letters (like 'CS' or 'BI')
                if(Char.IsDigit(requiredclass.CourseSubtext[2]))
                {
                    //Split the class subtext so it's easier to mess with
                    major = requiredclass.CourseSubtext.Substring(0, 2);
                    classnumber = Int32.Parse(requiredclass.CourseSubtext.Substring(2, 3));

                    //Get a list of all the classes from the major in question
                    classesInMajor = neededcourses.Where(m => m.CourseSubtext.Contains(major)).ToList();

                    //Go through and find the lowest numbered class for the course type (major? idk what the real word is)
                    foreach(var temp in classesInMajor)
                    {
                        if(Int32.Parse(temp.CourseSubtext.Substring(2, 3)) < lowestCourseNumber)
                        {
                            lowestCourseNumber = Int32.Parse(temp.CourseSubtext.Substring(2, 3));
                        }
                    }

                    //If there isn't already a class in the schedule list, add an entry for the lowest numbered class sequence
                    if (generatedSchedule.Where(m => m.CourseSubtext.Contains(major+lowestCourseNumber.ToString())).Count()==0)
                    {
                        generatedSchedule.Add(neededcourses.Where(m => m.CourseSubtext.Contains(lowestCourseNumber.ToString())).FirstOrDefault());
                    }
                }
                else //If the major code is 3 letters (like 'MTH' or 'COM')
                {
                    //Split the class subtext so it's easier to mess with
                    major = requiredclass.CourseSubtext.Substring(0, 3);
                    classnumber = Int32.Parse(requiredclass.CourseSubtext.Substring(3, 3));

                    //Get a list of all the classes from the major in question
                    classesInMajor = neededcourses.Where(m => m.CourseSubtext.Contains(major)).ToList();

                    //Go through and find the lowest numbered class for the course type (major? idk what the real word is)
                    foreach (var temp in classesInMajor)
                    {
                        if (Int32.Parse(temp.CourseSubtext.Substring(3, 3)) < lowestCourseNumber)
                        {
                            lowestCourseNumber = Int32.Parse(temp.CourseSubtext.Substring(3, 3));
                        }
                    }

                    if (generatedSchedule.Where(m => m.CourseSubtext.Contains(major + lowestCourseNumber.ToString())).Count() == 0)
                    {
                        generatedSchedule.Add(neededcourses.Where(m => m.CourseSubtext.Contains(lowestCourseNumber.ToString())).FirstOrDefault());
                    }

                }
            }


            /////////////////////////////////////////////
            /// CREATES A SCHEDULE WITH NEEDED CLASSES //
            /////////////////////////////////////////////

            /// Looks at the classes the user needs to take, then adds them to a list
            /// to display to the user.

            List<Cours> suggestedSchedule = new List<Cours>();
            List<UserSchedule> userScheduleItems = new List<UserSchedule>();
            userScheduleItems = db.UserSchedules.Where(m => m.UserID == userId).ToList();


            // Goes through the list of classes the user needs to take
            foreach (var totalclasses in generatedSchedule)
            {
                TimeSpan newStart = totalclasses.CourseStartTime;
                TimeSpan newEnd = totalclasses.CourseEndTime;
                string NewClassDays = DaysToBits(totalclasses.CourseDays);
                bool bad = false;
                bool scheduleFine = true;

                foreach (var individualItem in userScheduleItems)
                {
                    bool daySkip = false;
                    string OldClassDays = DaysToBits(individualItem.ActivityDays);
                    int checkDays = Int32.Parse(OldClassDays) + Int32.Parse(NewClassDays);

                    if (checkDays.ToString().Contains('2'))
                    {
                        //The days overlap, go on to check time
                    }
                    else
                    {
                        //The times don't overlap
                        daySkip = true;
                    }

                    if (daySkip == false)
                    {
                        // If the old class ends before the new one starts or starts after
                        // the new one ends...
                        if (
                            ((TimeSpan.Compare(individualItem.ActivityEndTime, newStart) == -1) ||  //Old class ends before new one starts
                            (TimeSpan.Compare(individualItem.ActivityStartTime, newEnd) == 1)) && //Old class starts after new one ends
                            ((TimeSpan.Compare(individualItem.ActivityStartTime, newStart) != 0) ||
                            (TimeSpan.Compare(individualItem.ActivityEndTime, newEnd) != 0))
                           )
                        { } //You're fine and can take both
                        else //The classes overlap and you can't take both
                        {
                            bad = true;
                            scheduleFine = false;
                        }
                    }
                }


                // If nothing's been added to the schedule yet, take the first class
                if (suggestedSchedule.Count() == 0 && scheduleFine == true)
                {
                    suggestedSchedule.Add(totalclasses);
                }
                else if (scheduleFine == true)
                {   
                    //Convert the days of the week string to bits (MWF is 0101010, TR is 0010100, etc)
                    

                    //TotalClasses: New Class (Checking if okay)
                    //Classes: Old Class (Comparing against)
                    foreach (var classes in suggestedSchedule)
                    {
                        bool daySkip = false;

                        //Convert the class your checking days-to-bits, then check if there's any overlap
                        //This is probably stupid, I'm sure there's a better way to do this, but it works.
                        string OldClassDays = DaysToBits(classes.CourseDays);
                        int checkDays = Int32.Parse(OldClassDays) + Int32.Parse(NewClassDays);

                        if(checkDays.ToString().Contains('2'))
                        {
                            //The days overlap, go on to check time
                        }
                        else
                        {
                            //The times don't overlap
                            daySkip = true;
                        }

                        if(daySkip==false)
                        {
                            // If the old class ends before the new one starts or starts after
                            // the new one ends...
                            if (
                                ((TimeSpan.Compare(classes.CourseEndTime, newStart) == -1) ||  //Old class ends before new one starts
                                (TimeSpan.Compare(classes.CourseStartTime, newEnd) == 1)) && //Old class starts after new one ends
                                ((TimeSpan.Compare(classes.CourseStartTime, newStart) != 0) ||
                                (TimeSpan.Compare(classes.CourseEndTime, newEnd) != 0  ))
                               )
                            {} //You're fine and can take both
                            else //The classes overlap and you can't take both
                            {
                                bad = true;
                            }
                        }
                    }
                    if(bad == false) //If it's not overlapping with any of your other classes, you can add the new one to your schedule
                    {
                        suggestedSchedule.Add(totalclasses);
                    }
                }
            }

            ViewBag.ClassesToTake = true;

            if(suggestedSchedule.Count()==0)
            {
                ViewBag.ClassesToTake = false;
            }

            return View(suggestedSchedule);
        }

        public ActionResult NeededCoursesOffered()
        {
            List<UserMajorRequirement> usermajorreqs = new List<UserMajorRequirement>();
            List<Cours> neededcourses = new List<Cours>();
            string userId = User.Identity.GetUserId();
            usermajorreqs = db.UserMajorRequirements.Where(m => m.UserID == userId && m.ReqMet == false).ToList();

            //Make a list of the current class IDs that the user is taking
            //(Then store it in the ViewBag)
            var userActivities = db.UserSchedules
                            .Where(u => u.UserID == userId)
                            .Select(a => a.ActivityCourseID);

            if (userActivities.Count() > 0)
            {
                List<int?> templist = userActivities.ToList();
                ViewBag.UserActivities = templist;
            }

            List<UserMajorRequirement> onlyclasses = new List<UserMajorRequirement>();
            String stringy = "";
            String[] delimit = new string[3];
            String concat = "";
            foreach (var entry in usermajorreqs)
            {
                stringy = entry.Requirement.RequirementTitle.ToString();

                if ((stringy[0] >= 'A' && stringy[0] <= 'Z' &&
                     stringy[1] >= 'A' && stringy[1] <= 'Z') &&
                     stringy.Substring(0, 2) != "BS")
                {
                    delimit = stringy.Split(' ');
                    concat = String.Concat(delimit[0], delimit[1]);
                    onlyclasses.Add(entry);


                    //Debug.WriteLine("The number is...");
                    //Debug.WriteLine(concat);

                    if (db.Courses.Where(m => m.CourseSubtext.ToString() == concat).FirstOrDefault() != null)
                    {
                        neededcourses.Add(db.Courses.Where(m => m.CourseSubtext.ToString() == concat).FirstOrDefault());

                        //Debug.WriteLine("The course title is...");
                        //Debug.WriteLine(db.Courses.Where(m => m.CourseSubtext.ToString() == concat).FirstOrDefault().CourseTitle);
                    }

                }

            }
            
            return View(neededcourses);
        }

        // GET: Cours/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Cours cours = db.Courses.Find(id);
            if (cours == null)
            {
                return RedirectToAction("NotFound", "Error");
            }

            var currentUserID = User.Identity.GetUserId();

            //Make a list of the current class IDs that the user is taking
            //(Then store it in the ViewBag)
            var userActivities = db.UserSchedules
                            .Where(u => u.UserID == currentUserID)
                            .Select(a => a.ActivityCourseID);

            List<int?> templist = userActivities.ToList();
            ViewBag.UserActivities = templist;
            ViewBag.ID = id;

            return View(cours);
        }



        public ActionResult Remove(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Cours cours = db.Courses.Find(id);
            if (cours == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(cours);
        }

        // POST: Cours/AddConfirmed/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Remove")]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveConfirmed(int? id)
        {
            ///////////////
            /// .SETUP.
            //////////////
            
            Cours cours = db.Courses.Find(id);
            AspNetUser currentUser = db.AspNetUsers.Find(User.Identity.GetUserId());
            UserSchedule userSchedule = (db.UserSchedules
                                .Where(s => s.ActivityCourseID.Equals(cours.ID))).FirstOrDefault();

            ///////////////
            /// .REMOVE SCHEDULE ITEM FROM DB AND DELETE THE GOOGLE CALENDAR EVENT.
            //////////////
            if (ModelState.IsValid)
            {
                db.UserSchedules.Remove(userSchedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userSchedule);
        }



        // GET: Cours/Add/5
        public ActionResult Add(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Cours cours = db.Courses.Find(id);
            if (cours == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(cours);
        }

        public static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }


        // POST: Cours/AddConfirmed/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Add")]
        [ValidateAntiForgeryToken]
        public ActionResult AddConfirmed(int? id)
        {

            ///////////////
            /// .SETUP.
            //////////////
            Cours cours = db.Courses.Find(id);
            AspNetUser currentUser = db.AspNetUsers.Find(User.Identity.GetUserId());

            Debug.WriteLine("Client stuff is...");

            /*
            string[] scopes = new string[] {
                 CalendarService.Scope.Calendar, // Manage your calendars
 	            CalendarService.Scope.CalendarReadonly // View your Calendars
             };


            String serviceAccountEmail = "studentschedulerserviceaccount@impactful-post-233615.iam.gserviceaccount.com";

            var certificate = new X509Certificate2("impactful-post-233615-60f9d6def4f6.p12", "notasecret", X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);

            ServiceAccountCredential credential = new ServiceAccountCredential(
                 new ServiceAccountCredential.Initializer(serviceAccountEmail)
                 {
                     Scopes = scopes
                 }.FromCertificate(certificate));



            var service = new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = "Student Scheduler Web",
            });*/

            if (ModelState.IsValid)
            {

                ///////////////
                /// .DAY OF THE WEEK PROCESSING.
                //////////////
                string RawDays = cours.CourseDays;
                var builder = new StringBuilder();
                for (int i = 0; i < RawDays.Length; i = i + 3)
                {
                    if (i > 0)
                    {
                        builder.Append(",");
                    }
                    if (RawDays[i] == 'M')
                    {
                        builder.Append("MO");
                    }
                    else if (RawDays[i] == 'T')
                    {
                        builder.Append("TU");
                    }
                    else if (RawDays[i] == 'W')
                    {
                        builder.Append("WE");
                    }
                    else if (RawDays[i] == 'R')
                    {
                        builder.Append("TH");
                    }
                    else if (RawDays[i] == 'F')
                    {
                        builder.Append("FR");
                    }
                    else if (RawDays[i] == 'S')
                    {
                        builder.Append("SA");
                    }
                    else if (RawDays[i] == 'U')
                    {
                        builder.Append("SU");
                    }
                }
                string CookedDays = builder.ToString();

                ///////////////
                /// .ADDING SCHEDULE EVENT TO DB.
                //////////////
                UserSchedule userSchedule = new UserSchedule
                {
                    UserID = User.Identity.GetUserId(),
                    ActivityTitle = cours.CourseTitle,
                    ActivityStartTime = cours.CourseStartTime,
                    ActivityEndTime = cours.CourseEndTime,
                    ActivityDescription = cours.CourseSubtext,
                    ActivityDays = cours.CourseDays,
                    ActivityCourseID = (int)id
                };

                ///////////////
                /// .CHECK IF USER HAS A GOOGLE CALENDAR.
                //////////////
                if (String.IsNullOrEmpty(currentUser.GoogleCalendarId))
                {
                    //If they don't have a calendar, we won't let them add classes to the calendar.
                    //For now.
                }
                else
                {/*
                    //Add event to calendar

                    ///////////////
                    /// .SET START AND END TIMES.
                    //////////////
                    DateTime start = new DateTime();
                    if(RawDays[0]=='M')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Monday);
                    }
                    else if(RawDays[0] == 'T')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Tuesday);
                    }
                    else if (RawDays[0] == 'W')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Wednesday);
                    }
                    else if (RawDays[0] == 'R')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Thursday);
                    }
                    else if (RawDays[0] == 'F')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Friday);
                    }
                    else if (RawDays[0] == 'S')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Saturday);
                    }
                    else if (RawDays[0] == 'U')
                    {
                        start = GetNextWeekday(DateTime.Today, DayOfWeek.Sunday);
                    }
                    start = start.Add(cours.CourseStartTime);
                    
                    
                    DateTime end = new DateTime();
                    if (RawDays[0] == 'M')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Monday);
                    }
                    else if (RawDays[0] == 'T')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Tuesday);
                    }
                    else if (RawDays[0] == 'W')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Wednesday);
                    }
                    else if (RawDays[0] == 'R')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Thursday);
                    }
                    else if (RawDays[0] == 'F')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Friday);
                    }
                    else if (RawDays[0] == 'S')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Saturday);
                    }
                    else if (RawDays[0] == 'U')
                    {
                        end = GetNextWeekday(DateTime.Today, DayOfWeek.Sunday);
                    }
                    end = end.Add(cours.CourseEndTime);

                    ///////////////
                    /// .BUILD THE WEEKLY REPETITION STRING.
                    //////////////
                    var builder2 = new StringBuilder();
                    builder2.Append("RRULE:FREQ=WEEKLY;BYDAY=");
                    builder2.Append(CookedDays);
                    builder2.Append(";INTERVAL=1");
                    string WeeklyRepeat = builder2.ToString();

                    ///////////////
                    /// .CREATE THE EVENT TO ADD TO GOOGLE CALENDAR.
                    //////////////
                    Event newEvent = new Event()
                    {
                        Description = cours.CourseSubtext,
                        Summary = cours.CourseTitle,
                        Recurrence = new String[] { WeeklyRepeat },

                        Start = new EventDateTime()
                        {
                            DateTime = start,
                            TimeZone = "America/Los_Angeles"
                        },

                        End = new EventDateTime()
                        {
                            DateTime = end,
                            TimeZone = "America/Los_Angeles"
                        },
                    };

                    ///////////////
                    /// .INSERT THE EVENT TO G CALENDAR AND SAVE THE ID FOR LATER DELETING IF NEEDED.
                    //////////////
                    var insertEvent = service.Events.Insert(newEvent, currentUser.GoogleCalendarId);
                    insertEvent.SendNotifications = true;
                    var SavedEvent = insertEvent.Execute();
                    var EventID = SavedEvent.Id;

                    userSchedule.GoogleCourseID = EventID; */
                }

                ///////////////
                /// .SAVE CHANGES TO DB.
                //////////////
                db.UserSchedules.Add(userSchedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", cours.MajorID);
            return View(cours);
        }

        // GET: Cours/Create
        public ActionResult Create()
        {
            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title");
            return View();
        }

        // POST: Cours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,MajorID,CourseTitle,CourseSubtext,CourseNumber,CourseDays,CourseStartTime,CourseEndTime,CreditCount,CourseAttributes,CourseRoom,CourseInstructor")] Cours cours)
        {
            if (ModelState.IsValid)
            {
                db.Courses.Add(cours);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", cours.MajorID);
            return View(cours);
        }

        // GET: Cours/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Cours cours = db.Courses.Find(id);
            if (cours == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", cours.MajorID);
            return View(cours);
        }

        // POST: Cours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MajorID,CourseTitle,CourseSubtext,CourseNumber,CourseDays,CourseStartTime,CourseEndTime,CreditCount")] Cours cours)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cours).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", cours.MajorID);
            return View(cours);
        }

        // GET: Cours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Cours cours = db.Courses.Find(id);
            if (cours == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(cours);
        }

        // POST: Cours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cours cours = db.Courses.Find(id);
            db.Courses.Remove(cours);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ShowClasses()
        {
             /* 
            List<string> Names = new List<string>();
            List<string> Subtexts = new List<string>();
            List<string> CRNs = new List<string>();
            List<string> Attributes = new List<string>();
            List<string> Credits = new List<string>(); //edit once going live
            List<string> Times = new List<string>();
            List<TimeSpan> STimes = new List<TimeSpan>();
            List<TimeSpan> ETimes = new List<TimeSpan>();
            List<string> Days = new List<string>();
            List<string> Classroom = new List<string>();
            List<string> Teacher = new List<string>();
            List<CouVM> CoInfo = new List<CouVM>();
            List<Cours> finalCourses = new List<Cours>();
            List<int> newCredits = new List<int>();

            string[] lines = System.IO.File.ReadAllLines(@"J:\\yellowCorp\\test.txt");
            foreach (string line in lines)
            {
                string[] sp = line.Split('|');
                Names.Add(sp[0]);
                Subtexts.Add(sp[1]);
                CRNs.Add(sp[2]);
                Attributes.Add(sp[3]);
                Credits.Add(sp[5]);
                Times.Add(sp[6]);
                if (sp[7].Contains("&nbsp;"))
                {
                    Days.Add("TBA");
                }
                else {
                    string daysCommed = "";
                    foreach (char letter in sp[7])
                    {
                        if (letter != sp[7][sp[7].Length - 1])
                        {
                            daysCommed = daysCommed + letter + ", ";
                        }
                        else
                        {
                            daysCommed = daysCommed + letter;
                        }
                    }
                    Days.Add(daysCommed);
                }
                Classroom.Add(sp[8]);
                if (sp[10].Length > 399)
                {
                    Teacher.Add(sp[10].Substring(0, 400));
                }
                else
                {
                    Teacher.Add(sp[10]);
                }
                //0,1,2,3,5,6,7,8,10
            }

            for (int i = 0; i < Names.Count(); i++)
            {
                if (Times[i].Contains("TBA"))
                {
                    STimes.Add(TimeSpan.Parse("00:00"));
                    ETimes.Add(TimeSpan.Parse("00:00"));
                }
                else if (Times[i].Contains('-'))
                {
                    System.Diagnostics.Debug.WriteLine(Times[i]);
                    string[] sp = Times[i].Split('-');
                    if (sp[0].Contains("am"))
                    {
                        int start = sp[0].IndexOf('a') - 1;
                        string actualStart = sp[0].Substring(0, start);
                        STimes.Add(TimeSpan.Parse(actualStart));
                    }
                    else if (sp[0].Contains("pm") && sp[0].Contains("12")==false)
                    {
                        int start = sp[0].IndexOf('p') - 1;
                        string actualStart = sp[0].Substring(0, start);
                        STimes.Add(TimeSpan.Parse(actualStart).Add(TimeSpan.FromHours(12)));
                    }
                    else
                    {
                        int start = sp[0].IndexOf('p') - 1;
                        string actualStart = sp[0].Substring(0, start);
                        STimes.Add(TimeSpan.Parse(actualStart));
                    }
                    if (sp[1].Contains("am"))
                    {
                        int start = sp[1].IndexOf('a') - 1;
                        string actualStart = sp[1].Substring(0, start);
                        ETimes.Add(TimeSpan.Parse(actualStart));
                    }
                    else if (sp[1].Contains("pm") && sp[1].Contains("12") == false)
                    {
                        int start = sp[1].IndexOf('p') - 1;
                        string actualStart = sp[1].Substring(0, start);
                        ETimes.Add(TimeSpan.Parse(actualStart).Add(TimeSpan.FromHours(12)));
                    }
                    else
                    {
                        int start = sp[1].IndexOf('p') - 1;
                        string actualStart = sp[1].Substring(0, start);
                        ETimes.Add(TimeSpan.Parse(actualStart));
                    }

                }
                else
                {
                    STimes.Add(TimeSpan.Parse("00:00"));
                    ETimes.Add(TimeSpan.Parse("00:00"));
                }
                //System.Diagnostics.Debug.WriteLine("{"+i+"}"+Credits[i]);
                // System.Diagnostics.Debug.WriteLine(Int32.Parse(Credits[i].Substring(0, 1)));
                newCredits.Add(Int32.Parse(Credits[i].Substring(0, 1)));

                CoInfo.Add(new CouVM
                {
                    cName = Names[i],
                    cSubtexts = Subtexts[i],
                    cCRN = CRNs[i],
                    cAtt = Attributes[i],
                    cCredits = newCredits[i],
                    cSTimes = STimes[i],
                    cETimes = ETimes[i],
                    cDays = Days[i],
                    cClassrrom = Classroom[i],
                    cInstructor = Teacher[i]
                });
            }
            
            try { 
            for (int i = 0; i < CoInfo.Count(); i++)
            {
                finalCourses.Add(new Cours
                {
                    MajorID = 1,
                    CourseTitle = Names[i],
                    CourseSubtext = Subtexts[i],
                    CourseNumber = CRNs[i],
                    CourseAttributes = Attributes[i],
                    CreditCount = newCredits[i],
                    CourseStartTime = STimes[i],
                    CourseEndTime = ETimes[i],
                    CourseDays = Days[i],
                    CourseRoom = Classroom[i],
                    CourseInstructor = Teacher[i]
                });
                db.Courses.Add(finalCourses[i]);
                System.Diagnostics.Debug.WriteLine("{0}:{1}:{2}:{3}", finalCourses[i].CourseTitle, finalCourses[i].CourseSubtext, finalCourses[i].CourseNumber, finalCourses[i].CourseDays);
                System.Diagnostics.Debug.WriteLine("Title: {0}, Sub: {1}, CRN: {2}, Att: {3}, Credits: {4}, Start: {5}, End: {6}, Days: {7}, Rooom: {8}, insturctor: {9}",finalCourses[i].CourseTitle, finalCourses[i].CourseSubtext, finalCourses[i].CourseNumber, finalCourses[i].CourseAttributes, finalCourses[i].CreditCount, finalCourses[i].CourseStartTime, finalCourses[i].CourseEndTime, finalCourses[i].CourseDays, finalCourses[i].CourseRoom, finalCourses[i].CourseInstructor);
                db.SaveChanges();
            }
        }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }   
            */
            return View();
        }
    }
}
