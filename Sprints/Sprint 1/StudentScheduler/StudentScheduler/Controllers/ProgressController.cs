﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Data;
using System.Data.Entity;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using StudentScheduler.Models.ViewModels;

namespace StudentScheduler.Controllers
{
    public class ProgressController : Controller
    {
        private SchedulerContext db = new SchedulerContext();
        // GET: Progress
        public ActionResult Index()
        {
            List<CompletionVM> notTake = db.Completions.Select(c => new CompletionVM
            {
                Requirements = c.Requirements,
                Subtexts = c.Subtexts,
                CreditsRequired = c.CreditsRequired,
                CreditsCompleted = c.CreditsCompleted,
                ClassesTaken = c.ClassesTaken
            }).ToList();
            return View(notTake);
        }
    }
}
