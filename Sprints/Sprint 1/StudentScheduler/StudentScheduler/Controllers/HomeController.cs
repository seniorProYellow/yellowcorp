﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentScheduler.Controllers
{
    public class HomeController : Controller
    {
        private SchedulerContext db = new SchedulerContext();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult InfoList()
        {
            if (User?.Identity.IsAuthenticated == true)
            { 
                var user = UserManager.FindById(User.Identity.GetUserId());
                if(user != null)
                {
                    ViewBag.MajorId = user.MajorId;
                }
            }
            ViewBag.Message = "A list of the different CRUDs";

            return View();
        }

        public JsonResult contactPush(string message, string email, string subject, string name)
        {
            Console.WriteLine("Your message: {0}, your email: {1}, your subject: {2}, your name: {3}",message,  email, subject, name);
            System.Diagnostics.Debug.WriteLine("Your message: {0}, your email: {1}, your subject: {2}, your name: {3}", message, email, subject, name);

            var newComment = new Comment
            {
                FullName = name,
                Email = email,
                FullSubject = subject,
                FullMessage = message
            };
            db.Comments.Add(newComment);
            db.SaveChanges();
            var data = new
            {

            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}