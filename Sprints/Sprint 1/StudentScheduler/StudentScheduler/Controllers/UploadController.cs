﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Data;
using System.Data.Entity;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
//using Excel = Microsoft.Office.Interop.Excel;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using StudentScheduler.Models.ViewModels;
using System.Runtime.InteropServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace StudentScheduler.Controllers
{
    public class UploadController : Controller
    {
        private ApplicationUserManager _userManager;
        private SchedulerContext db = new SchedulerContext();
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(object sender, EventArgs e, StudentScheduler.Models.Upload importExcel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string filepath = Server.MapPath("~/Content/" + importExcel.file.FileName);
                    importExcel.file.SaveAs(filepath);
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(filepath, false))
                    {
                        WorkbookPart wbPart = doc.WorkbookPart;
                        int worksheetcount = doc.WorkbookPart.Workbook.Sheets.Count();
                        Sheet mysheet = (Sheet)doc.WorkbookPart.Workbook.Sheets.ChildElements.GetItem(0);
                        Worksheet Worksheet = ((WorksheetPart)wbPart.GetPartById(mysheet.Id)).Worksheet;
                        int wkschildno = 4;
                        SheetData Rows = (SheetData)Worksheet.ChildElements.GetItem(wkschildno);
                        Row currentrow = (Row)Rows.ChildElements.GetItem(0);
                        Cell currentcell = (Cell)currentrow.ChildElements.GetItem(0);
                        List<Completion> comps = new List<Completion>();
                        string currentcellvalue = string.Empty;
                        for (int j = 1; j < Rows.Count(); j++)
                        {
                            string reqs = "";
                            string subtext = "";
                            int CredReq = 0;
                            int CredComp = 0;
                            string taken = "";
                            for (int i = 0; i < 5; i++)
                            {
                                currentrow = (Row)Rows.ChildElements.GetItem(j);
                                try
                                {
                                    currentcell = (Cell)currentrow.ChildElements.GetItem(i);
                                    if (currentcell.DataType != null)
                                    {
                                        //Console.Write("hey");
                                        if (currentcell.DataType == CellValues.SharedString)
                                        {
                                            int id = -1;

                                            if (Int32.TryParse(currentcell.InnerText, out id))
                                            {
                                                SharedStringItem item = GetSharedStrings(wbPart, id);

                                                if (item.Text != null)
                                                {
                                                    //code to take the string value  
                                                    currentcellvalue = item.Text.Text;
                                                    if (currentcellvalue.Contains(','))
                                                    {
                                                        int num;
                                                        num = Convert.ToInt32(currentcellvalue.Substring(0, currentcellvalue.LastIndexOf(',')));
                                                        if (i == 2)
                                                        {
                                                            CredReq = num;
                                                        }
                                                        else if (i == 3)
                                                        {
                                                            CredComp = num;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (i == 0)
                                                        {
                                                            reqs = currentcellvalue;
                                                        }
                                                        else if (i == 1)
                                                        {
                                                            subtext = currentcellvalue;
                                                        }
                                                        else if (i == 4)
                                                        {
                                                            taken = currentcellvalue; ;
                                                        }
                                                    }
                                                }
                                                else if (item.InnerText != null)
                                                {
                                                    currentcellvalue = item.InnerText;
                                                    if (i == 0)
                                                    {
                                                        reqs = currentcellvalue;
                                                    }
                                                    else if (i == 1)
                                                    {
                                                        subtext = currentcellvalue;
                                                    }
                                                    else if (i == 4)
                                                    {
                                                        taken = currentcellvalue; ;
                                                    }
                                                }
                                                else if (item.InnerXml != null)
                                                {
                                                    currentcellvalue = item.InnerXml;
                                                }
                                            }
                                            else
                                            {
                                            }
                                        }
                                    }
                                }
                                catch (Exception ec)
                                {
                                    taken = "";
                                }

                            }
                            Completion p = new Completion();
                            p.UserID = User.Identity.GetUserId();
                            p.Requirements = reqs;
                            p.Subtexts = subtext;
                            p.CreditsRequired = CredReq;
                            p.CreditsCompleted = CredComp;
                            p.ClassesTaken = taken;
                            comps.Add(p);
                            db.Completions.Add(p);
                            db.SaveChanges();
                        }

                    }
                    System.IO.File.Delete(filepath);
                }
                catch (Exception Ex)
                {

                    Console.WriteLine(Ex.Message);
                }
            }
            
            return View();
        }
        public ActionResult Progress()
        {
            string userId = User.Identity.GetUserId();
            IQueryable<CompletionVM> comps = null;
            IQueryable<CompletionVM> toDo = db.Completions.Select(c => new CompletionVM
            {
                UserID = c.UserID,
                Requirements = c.Requirements,
                Subtexts = c.Subtexts,
                ClassesTaken = c.ClassesTaken,
                CreditsCompleted = c.CreditsCompleted,
                CreditsRequired = c.CreditsRequired
            }).Where(com => com.CreditsCompleted < com.CreditsRequired).Where(com =>com.UserID == userId);
            if(toDo.Count() > 0)
            {
                ViewBag.toggle = 1;
                ViewBag.result = "These are the requirements you have not met yet";
            }
            return View(toDo.ToList());
        }
        public static SharedStringItem GetSharedStrings(WorkbookPart workbookPart, int id)
        {
            return workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
        }
    }
}