﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using Microsoft.AspNet.Identity;

namespace StudentScheduler.Controllers
{
    public class UserMajorRequirementsController : Controller
    {
        private ApplicationUserManager _userManager;
        private SchedulerContext db = new SchedulerContext();

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: UserMajorRequirements
        [Authorize(Roles = "Student")]
        public ActionResult Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var major = db.Majors.FirstOrDefault(m => m.ID == user.MajorId);
            var userMajorRequirements = db.UserMajorRequirements.Where(u => u.UserID == user.Id).Include(u => u.Requirement);
            ViewBag.Head = "My Requirements for " + major.Title + ":";
            return View(userMajorRequirements.ToList());
        }

        // POST: UserMajorRequirements/SetMetReqs
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Student")]
        [ValidateAntiForgeryToken]
        public ActionResult SetMetReqs(FormCollection form)
        {
            // Get collection of of selected met requirements
            var checkedValues = form.GetValues("reqMetChkBox");
            var userID = User.Identity.GetUserId();

            IQueryable<UserMajorRequirement> userMajorReqs = db.UserMajorRequirements.Where(umr => umr.UserID == userID);
            foreach ( UserMajorRequirement UMReq in userMajorReqs)
            {
                if (checkedValues == null)
                {
                    UMReq.ReqMet = false;
                }
                else
                {
                    UMReq.ReqMet = (Array.IndexOf(checkedValues, UMReq.ID.ToString()) > -1);
                }
                db.Entry(UMReq).State = EntityState.Modified;
            }
            db.SaveChanges();

            return RedirectToAction("InfoList","Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
