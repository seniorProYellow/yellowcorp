﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudentScheduler.DAL;
using StudentScheduler.Models;

namespace StudentScheduler.Controllers
{
    public class RequirementsController : Controller
    {
        private SchedulerContext db = new SchedulerContext();

        // GET: Requirements
        [Authorize(Roles = "Administrator,Staff,Student")]
        public ActionResult Index()
        {
            ViewBag.Title = "Requirements List";
            return View(db.Requirements.ToList());
        }

        // GET: Requirements/Details/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Requirement requirement = db.Requirements.Find(id);
            if (requirement == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(requirement);
        }

        // GET: Requirements/Create
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Create(int? id)
            //TODO: Pass in MajorId for using when posting back
        {
            var major = db.Majors.Find(id);
            if (major == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            else
            {
                ViewBag.MajorID = major.ID;
            }
                return View();
        }

        // POST: Requirements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Create([Bind(Include = "ID,RequirementTitle,RequirementDescription")] Requirement requirement)
        {
            // Check to see if Major ID is part of Post to redirect back to MajorRequirement Edit Page
            var majorId = Request["MajorID"];

            if (ModelState.IsValid)
            {
                db.Requirements.Add(requirement);
                db.SaveChanges();
                if (majorId == null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Create","MajorRequirements", new { id = majorId });
                }
                
            }

            return View(requirement);
        }

        // GET: Requirements/Edit/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Requirement requirement = db.Requirements.Find(id);
            if (requirement == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(requirement);
        }

        // POST: Requirements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Edit([Bind(Include = "ID,RequirementTitle,RequirementDescription")] Requirement requirement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requirement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(requirement);
        }

        // GET: Requirements/Delete/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Requirement requirement = db.Requirements.Find(id);
            if (requirement == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(requirement);
        }

        // POST: Requirements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult DeleteConfirmed(int id)
        {
            Requirement requirement = db.Requirements.Find(id);
            db.Requirements.Remove(requirement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
