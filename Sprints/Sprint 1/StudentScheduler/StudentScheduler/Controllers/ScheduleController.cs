﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using StudentScheduler.Extensions;
using System.Text;
using Microsoft.AspNet.Identity;
using System.Diagnostics;
using System.Data;

namespace StudentScheduler.Controllers
{
    public class ScheduleController : Controller
    {
        private SchedulerContext db = new SchedulerContext();

        public ActionResult Index()
        {

            //Get the users current schedule events
            var currentUserID = User.Identity.GetUserId();
            var userActivities = db.UserSchedules
                            .Where(u => u.UserID == currentUserID)
                            .Select(a => a);
            
           // Debug.WriteLine(userActivities.First().ActivityStartTime.ToString());


            //Add them to the events string
            var builder = new StringBuilder();
            builder.Append("[");
            Debug.WriteLine("The number of user activities?");
            Debug.WriteLine(userActivities.Count());
            int count = 0;
            int totalCount = userActivities.Count();

            foreach(var userevent in userActivities)
            {
                count++;
                builder.Append("{");

                //Adds the title
                builder.Append("\"title\":\"");
                builder.Append(userevent.ActivityTitle.ToString());

                builder.Append("\", \"dow\":");

                string RawDays = userevent.ActivityDays.ToString();
                builder.Append("[");
                for (int i = 0; i < RawDays.Length; i = i + 3)
                {
                    if (i > 0)
                    {
                        builder.Append(", ");
                    }
                    if (RawDays[i] == 'M')
                    {
                        builder.Append("1");
                    }
                    if (RawDays[i] == 'T')
                    {
                        builder.Append("2");
                    }
                    if (RawDays[i] == 'W')
                    {
                        builder.Append("3");
                    }
                    if (RawDays[i] == 'R')
                    {
                        builder.Append("4");
                    }
                    if (RawDays[i] == 'F')
                    {
                        builder.Append("5");
                    }
                    if (RawDays[i] == 'S')
                    {
                        builder.Append("6");
                    }
                    if (RawDays[i] == 'U')
                    {
                        builder.Append("0");
                    }
                }
                builder.Append("]");

                //Adds the start time
                builder.Append(", \"start\": \"");

                builder.Append(userevent.ActivityStartTime.ToString());

                //Set the end time
                builder.Append("\", \"end\": \"");

                builder.Append(userevent.ActivityEndTime.ToString());
                builder.Append("\"");

                //Set the color
                if(userevent.ActivityCourseID != null)
                {
                    builder.Append(", \"color\": \"#f46e6e\"");
                }
                else
                {
                    builder.Append(", \"color\": \"#76d8fc\"");
                }
                

                builder.Append("}");
                if(count!=totalCount)
                {
                    builder.Append(", ");
                }
            }


            builder.Append("]");
            Debug.WriteLine(builder.ToString());


            var model = new ScheduleViewModel
            {
                Events = builder.ToString()
            };

            return View(model);
        }
    }
}
