﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using StudentScheduler.Models;
using StudentScheduler.Extensions;
using StudentScheduler.DAL.Repositories.Abstract;
using System.Linq;
using StudentScheduler.DAL;

namespace StudentScheduler.Controllers
{
    public class MajorsController : Controller
    {
        private SchedulerContext db = new SchedulerContext();
        //private IMajorRepository majorRepository;
        //private ApplicationUserManager _userManager;
        public int? userSchoolId;
        private IUnitOfWork unitOfWork;

        private int GetUserSchoolId()
        {
            // userSchoolId will only already be set if testing
            if (userSchoolId != null)
            {
                return (int)userSchoolId;
            }
            else
            {
                // if user logged in use the user's school id, else zero for all
                if (User?.Identity.IsAuthenticated == true)
                {
                    return User.Identity.GetSchoolId();
                }
                else
                {
                    return 0;
                }
                    
            }
        }

        public MajorsController(IUnitOfWork myUnitOfWork)
        {
            unitOfWork = myUnitOfWork;
        }

        /*
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        */

        // GET: Majors
        public ActionResult Index()
        {
            //IQueryable<Major> majors = null;
            IEnumerable<Major> majors = null;
            var schoolId = GetUserSchoolId();
            if (schoolId == 0)
            {
                majors = unitOfWork.Majors.GetAll();
                ViewBag.Title = "Viewing majors for all schools:";
            }
            else
            {
                School school = unitOfWork.Schools.Get(schoolId);
                majors = unitOfWork.Majors.GetMajorsForSchool(GetUserSchoolId());
                //majors = unitOfWork.Majors.Find(m => m.SchoolID == schoolId);
                ViewBag.Title = "Viewing majors for " + school.Title + ":";
                ViewBag.SchoolId = school.ID;
            }

            //return View(majors.ToList());
            return View(majors);
        }

        // GET: Majors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //Major major = db.Majors.Find(id);
            Major major = unitOfWork.Majors.Get(id ?? 0);
            if (major == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(major);
        }

        // GET: Majors/Create
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Create()
        {
            var schoolId = GetUserSchoolId();

            if (User.IsInRole("Administrator"))
            {
                ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title");
            }
            else
            { 
                if (schoolId == 0)
                {
                    return RedirectToAction("NotFound", "Error");
                }
                else
                {
                    ViewBag.SchoolID = new SelectList(unitOfWork.Schools.Find(s => s.ID == schoolId), "ID", "Title");
                }
            }
            return View();
        }

        // POST: Majors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Create([Bind(Include = "ID,Title,SchoolID")] Major major)
        {
            if (ModelState.IsValid)
            {
                //db.Majors.Add(major);
                //db.SaveChanges();
                unitOfWork.Majors.Add(major);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title", major.SchoolID);
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title", major.SchoolID);
            return View(major);
        }

        // GET: Majors/Edit/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //Major major = db.Majors.Find(id);
            Major major = unitOfWork.Majors.Get(id ?? 0);
            if (major == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title", major.SchoolID);
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title", major.SchoolID);
            return View(major);
        }

        // POST: Majors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Edit([Bind(Include = "ID,Title,SchoolID")] Major major)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(major).State = EntityState.Modified;
                //db.SaveChanges();
                unitOfWork.Majors.Update(major);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title", major.SchoolID);
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title", major.SchoolID);
            return View(major);
        }

        // GET: Majors/Delete/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //Major major = db.Majors.Find(id);
            Major major = unitOfWork.Majors.Get(id ?? 0);
            if (major == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(major);
        }

        // POST: Majors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult DeleteConfirmed(int id)
        {
            //Major major = db.Majors.Find(id);
            //db.Majors.Remove(major);
            //db.SaveChanges();
            Major major = unitOfWork.Majors.Get(id);
            unitOfWork.Majors.Remove(major);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ShowAll()
        {
          /* 
            List<string> majorNames = new List<string>();
            List<string> hrefs = new List<string>();
            List<Major> Names = new List<Major>();

            string[] lines = System.IO.File.ReadAllLines(@"J:\\yellowCorp\\testMNames.txt");
            foreach (string line in lines)
            {
                string[] sp = line.Split('|');
                majorNames.Add(sp[0]);
                hrefs.Add(sp[1]);
            }

            for (int i = 0; i < majorNames.Count(); i++)
            {
                Names.Add(new Major
                {
                    Title = majorNames[i],
                    Href = hrefs[i],
                    SchoolID = 1
                });
                db.Majors.Add(Names[i]);
                db.SaveChanges();
            }


            //IQueryable<Major> majors = null;

            // Administrators can see Majors for all schools
            //majors = db.Majors.Include(m => m.School);

            */
            return View();//majors.ToList());
        }
    }
}
