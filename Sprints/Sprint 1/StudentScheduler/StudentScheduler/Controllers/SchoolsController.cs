﻿using System.Net;
using System.Web.Mvc;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.Models;

namespace StudentScheduler.Controllers
{
    public class SchoolsController : Controller
    {
        //private SchedulerContext db = new SchedulerContext();

        private IUnitOfWork unitOfWork;

        public SchoolsController(IUnitOfWork myUnitOfWork)
        {
            unitOfWork = myUnitOfWork;
        }

        // GET: Schools
        public ActionResult Index()
        {
            //return View(db.Schools.ToList());
            return View(unitOfWork.Schools.GetAll());
        }

        // GET: Schools/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //School school = db.Schools.Find(id);
            School school = unitOfWork.Schools.Get(id ?? 0);
            if (school == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(school);
        }

        // GET: Schools/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Schools/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create([Bind(Include = "ID,Title")] School school)
        {
            if (ModelState.IsValid)
            {
                //db.Schools.Add(school);
                //db.SaveChanges();
                unitOfWork.Schools.Add(school);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(school);
        }

        // GET: Schools/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //School school = db.Schools.Find(id);
            School school = unitOfWork.Schools.Get(id ?? 0);
            if (school == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(school);
        }

        // POST: Schools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "ID,Title")] School school)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(school).State = EntityState.Modified;
                //db.SaveChanges();
                unitOfWork.Schools.Update(school);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(school);
        }

        // GET: Schools/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            //School school = db.Schools.Find(id);
            School school = unitOfWork.Schools.Get(id ?? 0);
            if (school == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(school);
        }

        // POST: Schools/Delete/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //School school = db.Schools.Find(id);
            //db.Schools.Remove(school);
            //db.SaveChanges();
            School school = unitOfWork.Schools.Get(id);
            unitOfWork.Schools.Remove(school);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
