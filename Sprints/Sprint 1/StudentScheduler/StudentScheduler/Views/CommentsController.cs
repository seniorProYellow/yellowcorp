﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudentScheduler.DAL;
using StudentScheduler.Models;

namespace StudentScheduler.Views
{
    public class CommentsController : Controller
    {
        private SchedulerContext db = new SchedulerContext();

        // GET: Comments
        public ActionResult Index()
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            return View(db.Comments.ToList());
        }

        // GET: Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(comment);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FullName,Email,FullSubject,FullMessage")] Comment comment)
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(comment);
        }

        // GET: Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FullName,Email,FullSubject,FullMessage")] Comment comment)
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(comment);
        }

        // GET: Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            if (id == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsInRole("Administrator"))
            {
                return Redirect("/Home/Index");
            }
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
