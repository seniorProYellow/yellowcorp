namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MajorRequirement
    {
        public int ID { get; set; }

        public int MajorID { get; set; }

        public int ReqID { get; set; }

        public virtual Major Major { get; set; }

        public virtual Requirement Requirement { get; set; }
    }
}
