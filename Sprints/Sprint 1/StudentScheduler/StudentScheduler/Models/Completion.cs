﻿namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Completions")]
    public partial class Completion
    {
        public int ID { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("Requirements")]
        public string Requirements { get; set; }

        [StringLength(256)]
        [DisplayName("User ID")]
        public string UserID { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Subtexts")]
        public string Subtexts { get; set; }

        [Required]
        [DisplayName("Credits Required")]
        public int CreditsRequired { get; set; }

        [Required]
        [DisplayName("Credits Completed")]
        public int CreditsCompleted { get; set; }

        [DisplayName("Classes Taken")]
        [StringLength(200)]
        public string ClassesTaken { get; set; }

    }
}
