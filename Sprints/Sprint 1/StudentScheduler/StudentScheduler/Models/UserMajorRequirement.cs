namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserMajorRequirement
    {
        public int ID { get; set; }

        [Required]
        [StringLength(128)]
        public string UserID { get; set; }

        public int ReqID { get; set; }

        [DisplayName("Requirement Met")]
        public bool ReqMet { get; set; }

        //public virtual AspNetUser AspNetUser { get; set; }

        public virtual Requirement Requirement { get; set; }
    }
}
