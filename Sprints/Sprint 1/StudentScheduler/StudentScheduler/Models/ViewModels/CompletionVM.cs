﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StudentScheduler.Models;
using StudentScheduler.DAL;

namespace StudentScheduler.Models.ViewModels
{
    public class CompletionVM
    {
        public string UserID { get; set; }
        public string Requirements { get; set; }
        public string Subtexts { get; set; }
        public int CreditsRequired { get; set; }
        public int CreditsCompleted { get; set; }
        public string ClassesTaken { get; set; }

        //public List<Completion> CompList { get; set; }

    }
}