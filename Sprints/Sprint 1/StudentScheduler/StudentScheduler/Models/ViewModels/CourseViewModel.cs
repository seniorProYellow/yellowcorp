﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using StudentScheduler.Models;
using StudentScheduler.DAL;

namespace StudentScheduler.Models.ViewModels
{
    public class CourseViewModel
    {
        //Courses the user is taking
        //The courses
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
