﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentScheduler.Models.ViewModels
{
    public class MInfoVM
    {
        public string Mname { get; set; }
        public string hrefLink { get; set; }
        public int mNumber { get; set; }

        public List<MInfo> MajList { get; set; }
    }
}