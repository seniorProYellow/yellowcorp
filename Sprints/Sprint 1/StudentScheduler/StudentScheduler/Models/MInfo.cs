﻿namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    //[Table("Completions")]
    public partial class MInfo
    {
        public int ID { get; set; }

        [Required]
        [DisplayName("Major")]
        public string Mname { get; set; }

        [Required]
        [DisplayName("hrefLink")]
        public string hrefLink { get; set; }
        [Required]
        [DisplayName("School ID")]
        public string schoolNum { get; set; }
    }
}
