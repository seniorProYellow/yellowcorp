namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UniversityEvent
    {
        public int ID { get; set; }

        public int SchoolID { get; set; }

        [Required]
        [StringLength(100)]
        public string EventTitle { get; set; }

        [Required]
        [StringLength(400)]
        public string EventDescription { get; set; }

        [Required]
        [StringLength(100)]
        public string EventDays { get; set; }

        public TimeSpan EventStartTime { get; set; }

        public TimeSpan EventEndTime { get; set; }

        public virtual School School { get; set; }
    }
}
