USE Scheduler
GO

CREATE TABLE Schools
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	Title	 					VARCHAR(100)		NOT NULL,
	CONSTRAINT PK_Schools_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE Majors
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	Title	 					VARCHAR(100)		NOT NULL,
	Href						VARCHAR(200)			NULL,
	SchoolID					INT					NOT NULL,
	CONSTRAINT PK_Majors_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_Majors_ID		FOREIGN KEY (SchoolID)	
		REFERENCES Schools(ID)					
);
GO

CREATE TABLE Courses
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	MajorID						INT					NOT NULL,
	CourseTitle					VARCHAR(100)		NOT NULL,
	CourseSubtext				VARCHAR(400)		NOT NULL, --EDIT
	CourseNumber				VARCHAR(10)			NOT NULL,
	CourseDays					VARCHAR(100)		NOT NULL, --I still need to figure out how this would work for classes of multiple days
	CourseStartTime				TIME				NOT NULL,
	CourseEndTime				TIME				NOT NULL,
	CreditCount					INT					NOT NULL,
	CourseAttributes			VARCHAR(400)		NOT	NULL, --NEW
	CourseInstructor			VARCHAR(400)		NOT NULL, --NEW
	CourseRoom					VARCHAR(100)		NOT	NULL
	CONSTRAINT PK_CourseRequirements_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_CourseRequirements_ID		FOREIGN KEY (MajorID)	
		REFERENCES Majors(ID)
);
GO

CREATE TABLE Requirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	RequirementTitle			VARCHAR(100)		NOT NULL,
	RequirementDescription		VARCHAR(400)		NOT NULL
	CONSTRAINT PK_Requirements_ID		PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE MajorRequirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	MajorID						INT					NOT NULL,
	ReqID						INT					NOT NULL
	CONSTRAINT PK_MajorRequirements_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_MajorRequirements_ID		FOREIGN KEY (MajorID)	
		REFERENCES Majors(ID),
	CONSTRAINT FK_MajorRequirements2_ID		FOREIGN KEY (ReqID)	
		REFERENCES Requirements(ID)
);
GO

CREATE TABLE UserMajorRequirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	ReqID						INT					NOT NULL,
	ReqMet						BIT					NOT NULL
	CONSTRAINT PK_UserMajorRequirements_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

/*CREATE TABLE RequirementRules
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	ReqID						INT					NOT NULL,
	Course						NVARCHAR(255)		NOT NULL,
	AnyAll						CHAR(3)				NOT NULL,
	ReqCredits					INT					NOT NULL,
	CONSTRAINT PK_RequirementRules_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_RequirementRules_ID		FOREIGN KEY (ReqID)	
		REFERENCES Requirements(ID)
);
GO*/

CREATE TABLE UserSchedules
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	ActivityTitle				VARCHAR(100)		NOT NULL,
	ActivityDescription			VARCHAR(400)		NOT NULL,
	ActivityDays				VARCHAR(100)		NOT NULL, --I still need to figure out how this would work for activities of multiple days
	ActivityStartTime			TIME				NOT NULL,
	ActivityEndTime				TIME				NOT NULL,
	ActivityCourseID			INT					NULL,
	GoogleCourseID				VARCHAR(400)		NULL,
	CONSTRAINT PK_UserSchedules_ID		PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE UniversityEvents
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	SchoolID					INT					NOT NULL,
	EventTitle					VARCHAR(100)		NOT NULL,
	EventDescription			VARCHAR(400)		NOT NULL,
	EventDays					VARCHAR(100)		NOT NULL, --I still need to figure out how this would work for activities of multiple days
	EventStartTime				TIME				NOT NULL,
	EventEndTime				TIME				NOT NULL
	CONSTRAINT PK_UniversityEvents_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_UniversityEvents_ID		FOREIGN KEY (SchoolID)	
		REFERENCES Schools(ID)
);
GO

/***
This table is for recording a users progress, after they upload
their degree progress
***/
CREATE TABLE Completions
(
	ID							INT IDENTITY(1,1) NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	Requirements				VARCHAR(200)	NOT NULL,
	Subtexts					VARCHAR(100)	NOT NULL,
	CreditsRequired				INT				NOT NULL,
	CreditsCompleted			INT				NOT NULL,
	ClassesTaken				VARCHAR(200)	NOT NULL,
	CONSTRAINT PK_Completions_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE Comments
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	FullName	 				VARCHAR(200)		NOT NULL,
	Email		 				VARCHAR(100)		NOT NULL,
	FullSubject					VARCHAR(200)		NOT NULL,
	FullMessage					VARCHAR(400)		NOT NULL,
	CONSTRAINT PK_Comments_ID	PRIMARY KEY CLUSTERED (ID)
);
GO
/******  Added 5/8/2019  ******/
CREATE Table UserLogos
(
	ID							INT IDENTITY(1,1) NOT NULL,
	Link						NVARCHAR(400),
	CONSTRAINT PK_UserLogos_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[UserFullName] [nvarchar](128) NULL,
	[GoogleCalendarId] [nvarchar](256) NULL,
	[SchoolId] [int] NULL,
	[MajorId] [int] NULL,
	[UserLogo]	[nvarchar](400) NUll,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.Schools_ID] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[Schools] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.Schools_ID]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.Majors_ID] FOREIGN KEY([MajorId])
REFERENCES [dbo].[Majors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.Majors_ID]
GO

ALTER TABLE [dbo].[UserSchedules]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserSchedules_dbo.AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSchedules] CHECK CONSTRAINT [FK_dbo.UserSchedules_dbo.AspNetUsers_Id]
GO

--Constraints for UserMajorRequirements
--------------------------------------------------------------------------------------
ALTER TABLE [dbo].[UserMajorRequirements] WITH CHECK ADD CONSTRAINT [FK_dbo.UserMajorRequirements_dbo.AspNetUsers_ReqID] FOREIGN KEY ([UserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserMajorRequirements] CHECK CONSTRAINT [FK_dbo.UserMajorRequirements_dbo.AspNetUsers_ReqID]
GO

ALTER TABLE [dbo].[UserMajorRequirements] WITH CHECK ADD CONSTRAINT [FK_dbo.UserMajorRequirements_dbo.Requirements_ReqID] FOREIGN KEY ([ReqID])
REFERENCES [dbo].[Requirements] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserMajorRequirements] CHECK CONSTRAINT [FK_dbo.UserMajorRequirements_dbo.Requirements_ReqID]
GO
--------------------------------------------------------------------------------------
--Constraints for Completions
ALTER TABLE [dbo].[Completions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Completions_dbo.AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Completions] CHECK CONSTRAINT [FK_dbo.Completions_dbo.AspNetUsers_Id]
GO
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--Constraints for UserLogos
/*
ALTER TABLE [dbo].[UserLogos]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogos_dbo.AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserLogos] CHECK CONSTRAINT [FK_dbo.UserLogos_dbo.AspNetUsers_Id]
GO*/
----------------------------------------------------------------------------------------

INSERT dbo.Schools
(
	Title
)
VALUES
('Western Oregon University'),
('Oregon State University'),
('University of Oregon')
GO

INSERT dbo.Majors
(
	Title,
	Href,
	SchoolID
)
VALUES
('Computer Science, A.B./B.A./B.S.', 'http://www.wou.edu/academics/computer-science/', 1),
('Information Systems, A.B./B.A./B.S.', 'http://www.wou.edu/academics/information-systems/', 1),
('Visual Communications Design', 'http://www.wou.edu/academics/art-history-visual-communications-design-minor/', 1),
('Computer Science', NULL, 2)
GO

INSERT dbo.UniversityEvents
(
	SchoolID,
	EventTitle,
	EventDescription,
	EventDays,
	EventStartTime,
	EventEndTime
)
VALUES
(1, 'WOU Coffee Talk', 'Network with professionals in the field: ask questions, gain insight and learn more about your field.', 'March 12, 2019', '14:00:00', '16:00:00'),
(1, 'Smith Fine Art Series', 'Experience The Hot Club of San Fransico as it presents Cinema Vivant, an evening of silent films accompanied by live gypsy swing.','April 6, 2019', '16:30:00', '18:00:00'),
(1, 'Pow Wow', 'Experience Native American Culture with a ceremony involving dancing, singing, and feasting.', 'April 27, 2019', '12:00:00', '18:00:00'),
(1, 'WOU AES 2019', 'An entire day dedicated to the presentation of student scholarly activities.', 'May 30, 2019', '08:00:00', '16:00:00'),
(2, 'Spring Career Expo', 'Networking opportunity to meet prospect employers for expectant college graduates.', 'April 24, 2019', '11:00:00', '18:00:00'),
(3, 'The Illusionist', 'Experience hilarious magic trick, death-defying stunts, and acts of breathtaking wonder done by five of the most talented illusionists.', 'April 3, 2019', '12:00:00', '14:30:00')
GO

INSERT [dbo].[AspNetRoles] ([Id], [Name]) 
VALUES	(N'4a61e15a-5d32-4411-9e4f-8476baac9e4b', N'Student'),
		(N'6c5eaaba-7779-454a-a513-a8a83dfe8e41', N'Staff'),
		(N'7041742a-291b-49b1-8a5b-5909edec5c07', N'Administrator')
GO

INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [UserFullName], [GoogleCalendarId],[SchoolId],[MajorId],[UserLogo]) 
VALUES	(N'5118604e-9f46-414a-acdc-a00102db3150', N'student@test.com', 0, N'AChSRQ8obtlXN1PQfDlAjkl8JBtRwjNOgXCxxDTiF1IFSgCsoEW7aOOzuwApvYe4uw==', N'0199c446-dac4-4cbd-90bc-b218c78dbed6', NULL, 0, 0, NULL, 1, 0, N'student@test.com', 'Joe Studential', NULL, 1, NULL,'https://media.istockphoto.com/vectors/funny-happy-cute-happy-smiling-avocado-vector-id939768622?k=6&m=939768622&s=612x612&w=0&h=BevVyOpXKWlyoceMuvXAqZnjZZfl_7i0EpOPQSTo8Ig='),
		(N'88032915-db2a-4221-8878-2b571dc83c4f', N'staff@test.com', 0, N'AFG/qyukddXDQvRs/nYewOchEn5xMgAfjmXrBXymAlRAk0gmjWJayZhpVwOKBhI5CQ==', N'cc53347c-d72c-4a11-94ee-5d84f5a7b824', NULL, 0, 0, NULL, 1, 0, N'staff@test.com', 'Ann Staffer', NULL, 1, NULL,'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4_5EtIPLfG5WvAU0aUB1wlPODAfT3XXo4NlOvAEXlgrJAXUs4'),
		(N'edf1d47a-24bc-479c-b3af-2842e0d342ea', N'administrator@test.com', 0, N'AMqVUOtXxVaurtimzI7Dkp/D+zOm2E7IR8M4djMkAoZPvBcp1lEDrHM7MIogud1oyg==', N'e341a338-b9a2-4374-9d7c-87517278b03a', NULL, 0, 0, NULL, 1, 0, N'administrator@test.com', 'Elizabeth Adminieweather', NULL, NULL, NULL,'https://i.pinimg.com/originals/55/31/a4/5531a4d7815f295b475a1cb46ea46960.png')
GO

INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) 
VALUES	(N'5118604e-9f46-414a-acdc-a00102db3150', N'4a61e15a-5d32-4411-9e4f-8476baac9e4b'),
		(N'88032915-db2a-4221-8878-2b571dc83c4f', N'6c5eaaba-7779-454a-a513-a8a83dfe8e41'),
		(N'edf1d47a-24bc-479c-b3af-2842e0d342ea', N'7041742a-291b-49b1-8a5b-5909edec5c07')
GO

INSERT [dbo].[UserSchedules] ([UserID], [ActivityTitle], [ActivityDescription], [ActivityDays], [ActivityStartTime], [ActivityEndTime])
VALUES	(N'5118604e-9f46-414a-acdc-a00102db3150', 'Class', 'Going to class', 'M, W, F', '3:00', '5:00'),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 'Blah', 'Going to a different class', 'T, R', '8:00', '9:00')
GO

INSERT [dbo].[Requirements] ([RequirementTitle], [RequirementDescription])
VALUES
		(N'COM 111 Principles of Public Speaking', N'Completion of Principles of Public Speaking'),
		(N'Creative Arts', N'Complete at least 1 credit from three of the following program areas: art, dance, music, theater.'),
		(N'Health and Physical Education', N'Complete PE 131 plus one additional qualifying activity'),
		(N'Laboratory Science', N'Complete three lab science courses with at least'),
		(N'Literature', N'Complete at least 8 credits within the courses of ENG 104, ENG 105, or ENG 106.'),
		(N'Philosophy or Religion', N'Complete at least 3 credits within philosophy or religion. Course options are: PHL(101-103), PHL(251-253), PHL(261-263), or PHL(282-283)'),
		(N'Social Science', N'Complete one 8 or 9 credit social science sequence from ANTH(213-216), EC(201-202), GEOG(105-107), HST(104-105), HST(201-203), PS(201-204), SOC(223-225)'),
		(N'Writing', N'Complete course WR 122. Should be complete by end of sophomore year.'),
		(N'BS Writing intensive', N'Complete six credits of writing intensive'),
		(N'BS Cultural diversity', N'Complete six credits of coursework'),
		(N'BS Mathmatics/Computer Science', N'Complete a total of 11-12 credit hours in mathematics, computer science, and/or designated quantitative literacy courses. A minimum of one course in mathematics at the level of MTH 111 or above and one course in computer science (CS 121 or above) is required. The remaining required credit hours may be from either MTH, CS or designated quantitative literacy courses.'),
		(N'CS 160 Survey of Computer Science', N'Completion of Survey of Computer Science'),
		(N'CS 161 Introduction to Computer Science I', N'Completion of Introduction to Computer Science I'),
		(N'CS 162 Introduction to Computer Science II', N'Completion of Introduction to Computer Science II'),
		(N'CS 260 Data Structures', N'Completion of Data Structures'),
		(N'CS 271 Computer Organization', N'Completion of Computer Organization'),
		(N'CS 360 Programming Languages', N'Completion of Programming Languages'),
		(N'CS 361 Alrithms', N'Completion of Alrithms'),
		(N'CS 363 Information Assurance and Security', N'Completion of Information Assurance and Security'),
		(N'CS 364 Information Management', N'Completion of Information Management'),
		(N'CS 365 Operating Systems and Networking', N'Completion of Operating Systems and Networking'),
		(N'CS 460 Software Engineering I', N'Completion of Software Engineering I'),
		(N'CS 460 Software Engineering II', N'Completion of Software Engineering II'),
		(N'CS 460 Software Engineering III', N'Completion of Software Engineering III'),
		(N'MTH 231 Elements of Discrete Mathmatics I', N'Completion of Elements of Discrete Mathmatics I'),
		(N'MTH 232 Elements of Discrete Mathmatics II', N'Competion of Elements of Discrete Mathmatics II'),
		(N'MTH 354 Applied Discrete Mathmatics', N'Completion of Applied Discrete Mathmatics'),
		(N'Computer Science Electives Above 400', N'Complete 16 Credits in CS 4@'),
		(N'IS 240 Information Management', N'Completion of IS 240 Information Management'),
		(N'IS 270 Operating Systems', N'Completion of IS 270 Operating Systems'),
		(N'IS 278 Networks', N'Completion of IS 278 Networks'),
		(N'IS 340 Information Management II', N'Completion of IS 340 Information Management II'),
		(N'IS 345 Systems Analysis', N'Completion of IS 345 Systems Analysis'),
		(N'IS 350 Enterprise Architecture', N'Completion of IS 350 Enterprise Architecture'),
		(N'IS 355 Strategy, Acquisition and Management', N'Completion of IS 355 Strategy, Acquisition and Management'),
		(N'IS 440 Systems Administration', N'Completion of IS 440 Systems Administration'),
		(N'IS 470 Project Management', N'Completion of IS 470 Project Management'),
		(N'IS 475 Project Implementation', N'Completion of IS 475 Project Implementation'),
		(N'MTH 231 Elements of Discrete Mathematics I', N'Completion of MTH 231 Elements of Discrete Mathematics I'),
		(N'MTH 243 Introduction to Probability and Statistics', N'Completion of MTH 243 Introduction to Probability and Statistics'),
		(N'CS 123 Intro to IS or CS 160 Survey of CS', N'Completion of CS 123 Introduction to Information Systems OR CS 160 Survey of Computer Science'),
		(N'CS 161 Computer Science or CS13X Course', N'Completion of CS 161 Computer Science or one course numbered CS 13X programming language'),
		(N'Elective Credits ', N'At least 12 upper division credits:'),
		(N'A 220 Introduction to Typography', N'Completion of A 220 Introduction to Typography'),
		(N'A 262 Digital Images & Photography', N'Completion of A 262 Digital Images & Photography'),
		(N'Additional 200-Level Studio Courses', N'Minimum 8 additional credits of 200-level Art and Design courses'),
		(N'A 320 Graphic Design: Process & Theory', N'Completion of A 320 Graphic Design: Process & Theory'),
		(N'A 321 Graphic Design: Form and Communication', N'Completion of A 321 Graphic Design: Form and Communication'),
		(N'Additional Upper Division Courses', N'Minimum of 20 additional credits in 300- and/or 400-level Art and Design courses'),
		(N'A 315 Intermediate Design: Two-Dimensional', N'Completion of A 315 Intermediate Design: Two-Dimensional'),
		(N'A 316 Intermediate Design: Three-Dimensional', N'Completion of A 316 Intermediate Design: Three-Dimensional'),
		(N'A 429 Portfolio & Professional Preparation', N'Completion of A 429 Portfolio & Professional Preparation')
GO

INSERT [dbo].[MajorRequirements] ([MajorID], [ReqID])
VALUES
		(1, 1),
		(1, 2),
		(1, 3),
		(1, 4),
		(1, 5),
		(1, 6),
		(1, 7),
		(1, 8),
		(1, 9),
		(1, 10),
		(1, 11),
		(1, 12),
		(1, 13),
		(1, 14),
		(1, 15),
		(1, 16),
		(1, 17),
		(1, 18),
		(1, 19),
		(1, 20),
		(1, 21),
		(1, 22),
		(1, 23),
		(1, 24),
		(1, 25),
		(1, 26),
		(1, 27),
		(1, 28),
		(2, 1),
		(2, 2),
		(2, 3),
		(2, 4),
		(2, 5),
		(2, 6),
		(2, 7),
		(2, 8),
		(2, 9),
		(2, 10),
		(2, 29),
		(2, 30),
		(2, 31),
		(2, 32),
		(2, 33),
		(2, 34),
		(2, 35),
		(2, 36),
		(2, 37),
		(2, 38),
		(2, 39),
		(2, 40),
		(2, 41),
		(2, 42),
		(2, 43),
		(3, 1),
		(3, 2),
		(3, 3),
		(3, 4),
		(3, 5),
		(3, 6),
		(3, 7),
		(3, 8),
		(3, 9),
		(3, 10),
		(3, 44),
		(3, 45),
		(3, 46),
		(3, 47),
		(3, 48),
		(3, 49),
		(3, 50),
		(3, 51),
		(3, 52)
GO

INSERT [dbo].[UserMajorRequirements] ([UserID], [ReqID], [ReqMet])
VALUES
		(N'5118604e-9f46-414a-acdc-a00102db3150', 1, 1),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 2, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 3, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 4, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 5, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 6, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 7, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 8, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 9, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 10, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 11, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 12, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 13, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 14, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 15, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 16, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 17, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 18, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 19, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 20, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 21, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 22, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 23, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 24, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 25, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 26, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 27, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 28, 0)
GO

INSERT [dbo].[Courses] ([MajorID], [CourseTitle], [CourseSubtext], [CourseNumber], [CourseDays], [CourseStartTime], [CourseEndTime], [CreditCount], [CourseAttributes], [CourseRoom], [CourseInstructor])
VALUES  (1,'Ethics','Blah','12','M, W, F','14:00:00','15:00:00',4, 'test', 'room 4', 'bob'),
	    (1,'Journalism','Oh worm?','400','T, R','4:00:00','5:00:00',3, 'test', 'room 2', 'sally')
GO

INSERT dbo.UserLogos
(
	Link
)
VALUES
('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5AeoWZKee3824PbXpUax1ykFuYMntWsmgjLII-VZ8JLk38MWL4Q'),
('https://media.istockphoto.com/vectors/funny-happy-cute-happy-smiling-avocado-vector-id939768622?k=6&m=939768622&s=612x612&w=0&h=BevVyOpXKWlyoceMuvXAqZnjZZfl_7i0EpOPQSTo8Ig='),
('https://i.pinimg.com/originals/29/21/a2/2921a2c69e186f704f87435027ba4987.jpg'),
('https://i.pinimg.com/originals/55/31/a4/5531a4d7815f295b475a1cb46ea46960.png'),
('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT96T2n_43ofcl4I0at2Z7W_qwMMXpTqyBFLcgZIg5-dW6Cu2eG8g'),
('http://www.amisvegetarian.com/wp-content/uploads/2018/09/ice-cream-images-clip-art-ice-cream-clip-art-ice-cream-images-animations.png'),
('https://www.culinaryschools.org/clipart/rubber-duck.png'),
('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR71iJ9EV6796UzbdZyJ9XrI1OSja4gcMf86CavaBJGc4PZGXtXxg')
GO