﻿DROP TABLE Majors
GO

CREATE TABLE Majors
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	Title	 					VARCHAR(100)		NOT NULL,
	Href						VARCHAR(200)		NOT NULL,
	SchoolID					INT					NOT NULL,
	CONSTRAINT PK_Majors_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_Majors_ID		FOREIGN KEY (SchoolID)	
		REFERENCES Schools(ID)					
);
GO

INSERT dbo.Majors
(
	Title,
	Href,
	SchoolID
)
VALUES
/*('Computer Science', 1),
('Information Systems', 1),
('Visual Communications Design', 1),*/
('Computer Science','', 2)
GO