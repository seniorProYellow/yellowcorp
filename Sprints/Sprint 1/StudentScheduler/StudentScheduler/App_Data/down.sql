USE Scheduler
GO

DROP TABLE UniversityEvents
GO

DROP TABLE UserSchedules
GO

DROP TABLE RequirementRules
GO

DROP TABLE MajorRequirements
GO

DROP TABLE UserMajorRequirements
GO

DROP TABLE Requirements
GO

DROP TABLE Courses
GO

DROP TABLE AspNetUserClaims
GO

DROP TABLE AspNetUserLogins
GO

DROP TABLE AspNetUserRoles
GO

DROP TABLE AspNetRoles
GO

DROP TABLE AspNetUsers
GO


DROP TABLE Completions
GO

Drop TABLE	UserLogos
GO

Drop TABLE Comments
GO

DROP TABLE Majors
GO

DROP TABLE Schools
GO

IF OBJECT_ID('__MigrationHistory', 'U') IS NOT NULL 
	BEGIN
		DROP TABLE __MigrationHistory
	END

IF OBJECT_ID('UserAccounts', 'U') IS NOT NULL 
	BEGIN
		DROP TABLE UserAccounts
	END
