﻿Feature: Scheduler
	In order to organize my schedule, I would like to be able
	to see a potential list of classes I could take together next
	term. I should be able to add these classes to my schedule and
	view them, along with my personal schedule, on a calendar.

Scenario: Generate Schedule
	Given I have a school and major declared
	And I go to the generate schedule page
	Then the generate schedule page should show a list of non-conflicting classes that I can take from my major

Scenario: View Calendar
	Given that I have schedule items added
	Then I should be able to see my schedule items on the calendar

Scenario: Calendar Colors
	Given that I am signed in and have a major and school
	And also that I have classes added to my schedule
	And also that I have personal events added to my schedule
	Then the different categories of schedule item should be colored differently