﻿using System;
using TechTalk.SpecFlow;

namespace UnitTestStudentScheduler
{
    [Binding]
    public class SchedulerSteps
    {
        [Given(@"I have a school and major declared")]
        public void GivenIHaveASchoolAndMajorDeclared()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I go to the generate schedule page")]
        public void GivenIGoToTheGenerateSchedulePage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"that I have schedule items added")]
        public void GivenThatIHaveScheduleItemsAdded()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"that I am signed in and have a major and school")]
        public void GivenThatIAmSignedInAndHaveAMajorAndSchool()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"also that I have classes added to my schedule")]
        public void GivenAlsoThatIHaveClassesAddedToMySchedule()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"also that I have personal events added to my schedule")]
        public void GivenAlsoThatIHavePersonalEventsAddedToMySchedule()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the generate schedule page should show a list of non-conflicting classes that I can take from my major")]
        public void ThenTheGenerateSchedulePageShouldShowAListOfNon_ConflictingClassesThatICanTakeFromMyMajor()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should be able to see my schedule items on the calendar")]
        public void ThenIShouldBeAbleToSeeMyScheduleItemsOnTheCalendar()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the different categories of schedule item should be colored differently")]
        public void ThenTheDifferentCategoriesOfScheduleItemShouldBeColoredDifferently()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
