﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentScheduler;
using StudentScheduler.Controllers;
using System.Text;

namespace UnitTestStudentScheduler.ControllerTests
{
    [TestClass]
    public class HomeController_Test
    {
        [TestMethod]
        public void Index()
        {
            // Arange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.AreEqual("Your contact page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void InfoList()
        {
            // Arange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.InfoList() as ViewResult;

            // Assert
            Assert.AreEqual("A list of the different CRUDs", result.ViewBag.Message);
        }



    }
}
