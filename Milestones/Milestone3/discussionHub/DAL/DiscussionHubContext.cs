namespace discussionHub.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using discussionHub.Models;

    public partial class DiscussionHubContext : DbContext
    {
        public DiscussionHubContext()
            : base("name=DiscussionHubContext")
        {
        }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Discussion> Discussions { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .Property(e => e.commentContents)
                .IsUnicode(false);

            modelBuilder.Entity<Discussion>()
                .Property(e => e.discussionTitle)
                .IsUnicode(false);

            modelBuilder.Entity<Discussion>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Discussion)
                .HasForeignKey(e => e.commentDiscussionID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.userName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.userEmail)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.userCountry)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.userStateProvince)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.userPoliticalParty)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.userPassword)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.commentUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Discussions)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.discussionUserID)
                .WillCascadeOnDelete(false);
        }
    }
}
