SELECT	*
FROM	Users

SELECT	*
FROM	Discussions

SELECT	*
FROM	Comments

SELECT	discussionID,
		discussionTitle,
		discussionDateTime,
		discussionActive,
		DU.userName AS DiscussionCreator,
		commentID,
		CU.userName AS CommentCreator,
		commentDateTime,
		commentContents
		
FROM	Discussions
		LEFT OUTER JOIN Comments ON (discussionID = commentDiscussionID)
		LEFT OUTER JOIN Users CU ON (commentUserID = CU.userID)
		LEFT JOIN Users DU ON (discussionUserID = DU.userID)

ORDER BY	discussionID, commentDateTime