namespace discussionHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Comment
    {
        public int commentID { get; set; }

        public int commentDiscussionID { get; set; }

        public int commentUserID { get; set; }

        public DateTime commentDateTime { get; set; }

        [Required]
        public string commentContents { get; set; }

        public virtual Discussion Discussion { get; set; }

        public virtual User User { get; set; }
    }
}
