namespace discussionHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Comments = new HashSet<Comment>();
            Discussions = new HashSet<Discussion>();
        }

        public int userID { get; set; }

        [Required]
        [StringLength(100)]
        public string userName { get; set; }

        [Column(TypeName = "date")]
        public DateTime userDOB { get; set; }

        [Required]
        [StringLength(100)]
        public string userEmail { get; set; }

        [Required]
        [StringLength(75)]
        public string userCountry { get; set; }

        [Required]
        [StringLength(100)]
        public string userStateProvince { get; set; }

        [Required]
        [StringLength(100)]
        public string userPoliticalParty { get; set; }

        [Required]
        [StringLength(25)]
        public string userPassword { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Discussion> Discussions { get; set; }
    }
}
