# Welcome to The Student Scheduler

## Description:
Though scheduling in college is vital, tools provided to help students determine their schedules are often lacking. The Student Scheduler is a project that aims to synthesize a students needs along with what they’ve already completed and help students determine which classes to take in future terms.

The Student Scheduler also allows for users to enter in non-scholastic schedule items when deciding which classes to take, allowing for more flexibility and utility in the schedules. To research this concept we looked at existing competing software like DegreeWorks and considered where they fell short for most students, then planned on ways to improve those problems.

Overall, we aimed to simplify the process of choosing which classes to register for as a student in order to minimize inefficient class selection and to hopefully enhance the experience so as to be less stressful for the user. Created by us, Yellow Umbrella Corp, for Western Oregon University's senior software design sequence.


## Guidelines:
* Always make sure to pull development branch before starting a feature branch
* Feature branch name style: ft_numberOfPBI_yourName (ex: ft_104_bob)
* Primary key for tables will use “ID” style, not “ProdcutID”
* Write XML comments for all public methods.
* Primary language will be C\#


## Team Members: 
* Joshua Martinez
* Schaefer Jones
* Autumn Greenley
* Launia Davis


## Software Construction:
* Agile Software Construction Process
* Disciplined Agile Delivery Lifecycle 


## Team Properties:
### Rules: 
What happens in Yellow Umbrella Corp., stays in Yellow Umbrella Corp. 
### Song: 
It's Not Unusual - Tom Jones
### Cheer: 
Yellow Umbrella Cheer (Hillshire Farms Tune)
### Dance Move: 
Carlton Dance
### Mascot: 
Umbrella with eyes


## Tools:
* Bitbucket
* Git
* The internet
* Schedule of classes from Western Oregon University
* Graduation requirements from Western Oregon University
* Azure DevOps
* Visual Studio
* Discord
* Google Docs


## Installations:
* Bootstrap 4
* Entity Framework 6
* jQuery

## ER Database Diagram
Here is our ER Database Diagram as well as a [link](https://bitbucket.org/seniorProYellow/yellowcorp/src/master/ER Diagram.png) to the source image.
![Diagram](https://bitbucket.org/seniorProYellow/yellowcorp/src/master/ER Diagram.png "ER Database Diagram")

## How to run
To create your own instance of the Student Scheduler, clone this repository to your local machine. From there, you'll have to create a local database and set up your own connection string to that database in the web.config. Finally just run the upscript (in App_Data) to populate your local DB, which should leave you with a functional local version of the site.

